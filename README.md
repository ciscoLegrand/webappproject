WEBAPP Red Social educativa
===========================

Resumen del proyecto
------------------------------------------------------------------------

* Resumen rápido

Red Social similar a instagram a nivel local o por grupos en la que los usuarios puedan desarrollar aptitudes y usos responsables de las rs ya existentes.

* Version: **v0.0.0**

Configuración
-------------

* Resumen de configuración
* Estructura del repositorio
    + **master**: **rama principal**
    + **develop**: **rama de desarrollo**
    + **opcional pruebas**
        + **[nombre_rama]**: **rama de trabajo y pruebas personal**

* Dependencias
  > path/to/service/package.json 

* Configuración de base de datos
    + **instalación local MongoDB**
        + Actualizar paquetes:
      > sudo apt update && sudo apt upgrade -y 
        + Instalar MongoDB
      > sudo apt install mongodb 
        + Verificar estado:
      > sudo systemctl status mongodb 
      > sudo systemctl stop mongodb 
      > sudo systemctl start mongodb 
      > sudo systemctl restart mongodb 
        + Configurar si MongoDB inicia automáticamente cuando se inicia el sistema o no
      > sudo systemctl disable mongodb 
      > sudo systemctl enable mongodb 
        + Iniciar shell de mongo 
      > mongo 
        + Desinstalar MongoDB
      > sudo systemctl stop mongodb 
      > sudo apt purge mongodb 
      > sudo apt autoremove 
    + **"instalación" MongoDB dockerizado**
      > docker run --name [nombre_contenedor] -p 27017:27017 -d mongo 
      > docker exec -ti [nombre_contenedor] bash 
      > mongo 
    + **MongoDB en la nube**
      [gurdar datos en cloud.mongodb.com](https://cloud.mongodb.com/)

    * Básicos mongodb:
        + ver bases de datos:
        > show databases || show dbs 
        + usar/crear/cambiar una base de datos (si no tiene datos no se mostrará)
        > use [nombre_bd]
        + insertar datos
        > db.[coleccion].insert([documento_json]) 
        + ver colecciones de una base de datos:
        > show collections 
        + consultar datos en una colección:
        > db.[coleccion].find() 
        > db.[coleccion].find().pretty() #datos formateados 


* Cómo ejecutar pruebas
* Instrucciones de implementación

Pautas de contribución
----------------------

* Primeros pasos
    - **Clonando** el repositorio:
      > git clone <url_github> 
    - Descargando **ramas** al ***working directory***:
      > git pull origin <rama> 
    - Añadiendo un **repo remoto**:
      > git remote add <url_github> 

* Pasos para subir cambios
    - Ramas:
          + **master**: **rama principal** donde se encuentra la **version estable**, lo que se ponga en esta rama debe ser funcional.
      > git push origin master 
          + **develop**: **rama de desarrollo** que surge a partir de **master**, en ella se icorporan las nuevas caracteristicas para integrar con la rama master una vez aprobadas.
      >git push origin develop 
          + **[nombre_rama]**: **rama personal** en la que trabajar para realizar pruebas eltenativas o si se quiere esperar a tener codigo funcionaly una vez completado integrarlo con la rama **develop** para su revisión.
      >git push origin [nombre]
      
          - Como **crear rama** || crear rama y **moverse** a la ***rama creada***
        > git branch [nombre_de_rama] || git checkout -b [nombre_de_rama] 
          - Como movernos entre ramas:
        > git checkout [nombre_de_rama] 

    - Comandos para **commit**:
          1. **visualizar** los cambios en el **working directory** y en la **staging area** ***comparado con el repositorio***:
      > git status 
          2. **agregar** un **archivo** a **staging area** || agregar todos los archivos a staging area
      > git add [nombre del archivo] 
      > git add . (posibles conflictos) 
          3. ***commitear un archivo*** y escribir ***mensaje*** en terminal || commitear todos los archivos y escribir mensaje en terminal
      > git commit [nombre del archivo] -m '<mensaje>' 
      > git commit -am '<mensaje>' (posibles conflictos) 

      abre el editor de texto por defecto para escribir el mensaje del commit.

      > git commit [nombre del archivo] 
      > git commit -a 

    - **Mensaje** commit:
          + Comenzar con letra mayuscula
          + terminar sin punto
          + no debe contener más de 50 caracteres(recomendado)
          + debe explicar que cambios se realizaron

      > p.e: git commit -m 'Implementa llamada a la base de datos' [nombre del archivo] 

    - Versionado: se empleara ***vX.Y.Z***
          + X: version **principal** del software
          + Y: **nuevas funcionalidades**
          + Z: **revision de codigo** por algun fallo, correccion de bugs.
          + para **crear** el **tag** de la version || crear tag con mensaje
          > git tag vX.Y.Z || git tag -a vX.Y.Z -m "<mensaje_de_la_version>" 
          + para **subir** el **tag** a origin (github)
          > git push origin <tag> || ej: git push origin v0.0.0 

* Nombres documentos

* Otras pautas

      + **comentar** en lo posible el **codigo**, que hacen las funciones, que parametros reciben ...
      + Clases: los **nombres** de las **clases**  deben ser ***substantivos*** en `UpperCamelCase`, usar ***palabras completas***.
      > p.e: Post.rb || User.model.js 
      + variables: los **nombres** de las **variables** deben estar en `lowerCamelCase` y deben ser ***significativos***.
      > p.e: String nombre   ||   user   ||   let foto   || 
      > mal:           nom   ||   u      ||   let f      || 
 
      + Métodos: los **nombres** de los **métodos** deben estar en `lowerCamelCase` y ***empezar con un verbo*** y que sea ***descriptivo***.
      > p.e: creaNuevoPost()   ||   muestraInfoDelUsuario()   ||   ordenaListaDeSeguidoresAlfabeticamente() 

      - carpetas y **directorios**:
            + si son de mas de una palabra **separada** con barra baja **"_"**.
            > p.e: imagenes_banner_contact

      - **comunicacion** interna: [trello.com] (https://trello.com/b/YNEBq8xx/webapp)

Contacto
--------

* **Propietario** del repositorio 
    - creadix

* **Contacto** equipo: ***creadix@creadix.es***
